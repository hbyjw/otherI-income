package com.feixunruanjian.myplugins;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.Image;
import android.os.Build;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.LOG;
import org.apache.cordova.PermissionHelper;
import org.apache.cordova.PluginResult;
import org.apache.cordova.filetransfer.FileTransfer;
import org.json.JSONArray
        ;
import org.json.JSONException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

/**
 * Created by hbyjw on 2017/7/24 0024.
 */

public class ImgProcessing extends CordovaPlugin {
    /**
     * JS回调接口对象
     */
    public static CallbackContext cbCtx = null;
    private static final int IMAGE_PICK = 0;// /相册 标记
    private static final int IMAGE_CAMERA = IMAGE_PICK + 1;// /相册 标记

    private static final int DIALOG_INDEX = 0;// 对话框标记
    private int mProgress = 0;// 进度条
    private boolean isUP_LOAD_IMAGE = false; // 判断用户是否取消上传
    private ProgressDialog pgDialog = null;
    private AlertDialog.Builder mAlertDialog = null;
    private int needheight=0;//需要缩放像素的高度
    private int needwidth=0;//需要缩放像素的宽度
    private File fileName = null;
    private int wmflg=0;//是否需要添加水印,0表示否，1表示是
    private int wmfz=20;//水印字体大小
    private String wmfc="red";//水印字体颜色
    private String wmtxt="";//水印字符串
    //private String urlStr = "/ajaxfileupload.ashx";// URL 后半段
    private String httpUrlPath = null; // 拼接之后的url
    private String ImgPath=null;//图片在安卓里面的文件路径

    private String groupno="";//本地保存图片的分组号 ""表示不需要本地保存

    /**
     * 插件主入口
     */
    @Override
    public boolean execute(String action, final JSONArray args, CallbackContext callbackContext) throws JSONException {
        cbCtx = callbackContext;
        JSONObject myJsonObject = new JSONObject(args.getString(0));
        httpUrlPath = myJsonObject.getString("url");//文件上传的后台接口地址，因为换用了上传方式，可以不用该变量了
        ImgPath=myJsonObject.getString("imgpath");//获取要上传的文件的路径
        try{
            if(myJsonObject.has("groupno"))
            {
                groupno=myJsonObject.getString("groupno");
            }
            //获取缩放参数
            String[] zoomarg=myJsonObject.getString("zoom").split(",");
            needwidth=Integer.parseInt(zoomarg[0]);
            needheight=Integer.parseInt(zoomarg[1]);
        }catch(Exception e)
        {
            showError("获取缩放参数错误,请检查传入参数是否正确!"+e.getMessage());
            return true;
        }
        //获取设置上传图片的水印参数
        try
        {
            //获取水印设置参数
            String[] watermark=myJsonObject.getString("watermark").split(",");
            wmflg=Integer.parseInt(watermark[0]);

            wmfz=Integer.parseInt(watermark[1]);
            wmfc=watermark[2].toString();
            wmtxt=watermark[3].toString();
        }catch(Exception e)
        {
            showError("获取水印参数错误,请检查传入参数是否正确!"+e.getMessage());
            return true;

        }

           //判断传递进来是否要做图片处理(ImgProcessing)操作
        if ("ImgProcessing".equalsIgnoreCase(action)) {
            if (!needsToAlertForRuntimePermission()) {
               // performGetLocation();
                //开始读取文件进行图片处理
                Processing(ImgPath);

            } else {
                //请求权限
                requestPermission(ImgPath);
                // 会在onRequestPermissionResult时performGetLocation
            }
            return true;
        }

        return false;
    }

    private static final int REQUEST_CODE = 100001;

    private boolean needsToAlertForRuntimePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
         //   boolean b1=cordova.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE);
          //  boolean b2=cordova.hasPermission(Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS);
            //READ_EXTERNAL_STORAGE
            return !cordova.hasPermission(Manifest.permission.READ_EXTERNAL_STORAGE) ||!cordova.hasPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) || !cordova.hasPermission(Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS);
        } else {
            return false;
        }
    }

    private void requestPermission(String uri) {

        String[] permissions=new  String[3];
        permissions[0]="Manifest.permission.MOUNT_UNMOUNT_FILESYSTEMS";
        permissions[1]="Manifest.permission.WRITE_EXTERNAL_STORAGE";
        permissions[2]="Manifest.permission.READ_EXTERNAL_STORAGE";
        PermissionHelper.requestPermissions(this, 1, permissions);
       // PermissionHelper.requestPermission(this, 1, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        Processing( uri);
    }
    //对图片进行处理
    private void Processing(String uri)
    {
        //开始处理图片
        String bs="";
        uri=uri.replace("file:///","");
        //获取到图片
        File newfile=new File(uri);
       // float fl= newfile.length();
        /*
        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;
        Bitmap bitmap=BitmapFactory.decodeFile(uri,opt);
        */
        /*start获取完整的图片路径，以及要处理后的文件保存路径*/
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "IMG_" + timeStamp +".jpg" ;
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        String galleryPath = storageDir.getAbsolutePath() + "/" + imageFileName;
        /*end获取完整的图片路径，以及要处理后的文件保存路径*/
        //开启新的县城处理图片
        startThreadCompressImage(newfile,galleryPath,newfile.length());
    }

    /**
     * 启动新线程压缩处理图片
     * @param imageFile
     * @param uploadPhotPath
     * @return
     */
    private void startThreadCompressImage(final File imageFile,final String uploadPhotPath ,final float fileLength)
    {
        new Thread()
        {
            public void run()
            {
                //上传前对图片进行压缩
                Bitmap bitmap=readBitMap(imageFile);
                File newfile=new File(uploadPhotPath);//将要保存图片的路径
                if(needheight>0)
                {
                    //对图片进行等比缩放
                    bitmap=	zoomImg(bitmap,needheight,needwidth);
                }

                //  bitmap=Watermarkbmp;
                //判断是否需要添加水印
                if(wmflg==1)
                {
                    //压缩后的图片添加水印
                    wmfc=wmfc.toUpperCase();
                    int fci=0xFFFF0000;//默认黑色
                    //获取文字颜色
                    if(wmfc.equals("BLACK"))
                    {
                        fci=0xFF000000;
                    }else if (wmfc.equals("DKGRAY"))
                    {
                        fci=0xFF444444;
                    }else if (wmfc.equals("GRAY"))
                    {
                        fci=0xFF888888;
                    }else if (wmfc.equals("LTGRAY"))
                    {
                        fci=0xFFCCCCCC;
                    }else if (wmfc.equals("WHITE"))
                    {
                        fci=0xFFFFFFFF;
                    }else if (wmfc.equals("RED"))
                    {
                        fci=0xFFFF0000;
                    }else if (wmfc.equals("GREEN"))
                    {
                        fci=0xFF00FF00;
                    }else if (wmfc.equals("BLUE"))
                    {
                        fci=0xFF0000FF;
                    }else if (wmfc.equals("YELLOW"))
                    {
                        fci=0xFFFFFF00;
                    }else if (wmfc.equals("CYAN"))
                    {
                        fci=0xFF00FFFF;
                    }else if (wmfc.equals("MAGENTA"))
                    {
                        fci=0xFFFF00FF;
                    }else if (wmfc.equals("TRANSPARENT"))
                    {
                        fci=0;
                    }
                    /**
                     * 添加水印
                     *
                     * @param bitmap       原图
                     * @param markText     水印文字
                     * @param fontsize     文字大小
                     * @param fontcolor    文字颜色
                     * @return bitmap      打了水印的图
                     */
                    bitmap=createWatermark(bitmap,wmtxt,wmfz,fci);
                }
                //这里面开始进行图片压缩
                try {
                    FileOutputStream out=new FileOutputStream(newfile);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                    int options = 80;
                    while (baos.toByteArray().length > 1024 * 1024) { // 循环判断如果压缩后图片是否大于1000kb,大于继续压缩
                        LOG.v("baos.toByteArray.length", baos.toByteArray().length + "");
                        baos.reset(); // 重置baos即清空baos
                        bitmap.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
                        if(options>=30)
                        {
                            options -= 10;// 每次都减少10
                        }else
                        {
                            bitmap.compress(Bitmap.CompressFormat.JPEG, options, baos);
                            break;
                        }
                    }
                    out.write(baos.toByteArray());
                    out.flush();
                    out.close();


                    //根据情况判断是否需要,如果是详细拍照的那么删除，如果是相册选择的
                    imageFile.delete();

                    if(groupno.equals(""))
                    {
                        //返回压缩后的图片路径，然后调用文件上传插件进行上传
                        showsuccess(uploadPhotPath);

                    }else
                    {

                            showsuccess(imageToBase64(uploadPhotPath));
                    }


                 //   startThreadUpLoadImage(newfile, fileLength);

                } catch (IOException e) {
                    e.printStackTrace();
                    //压缩失败已原图方式上传
                    showsuccess(imageFile.getAbsolutePath());
                    //startThreadUpLoadImage(imageFile, fileLength);
                }


            }
        }.start();
    }
    /**
     * 将图片转换成Base64编码的字符串
     * @param path
     * @return base64编码的字符串
     */
    public static String imageToBase64(String path) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
            is = new FileInputStream(path);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        Log.v("文件长度", String.valueOf(result.length()));
        return result;
    }
    /**
     * 添加水印
     *
     *
     * @param bitmap       原图
     * @param markText     水印文字
     * @param fontsize     文字大小
     * @return bitmap      打了水印的图
     */
    public static Bitmap createWatermark(Bitmap bitmap, String markText,int fontsize,int fontcolor) {

        // 当水印文字与水印图片都没有的时候，返回原图
        if (TextUtils.isEmpty(markText)) {
            return bitmap;
        }
        // 获取图片的宽高
        int bitmapWidth = bitmap.getWidth();
        int bitmapHeight = bitmap.getHeight();

        // 创建一个和图片一样大的背景图
        Bitmap bmp = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmp);
        // 画背景图
        canvas.drawBitmap(bitmap, 0, 0, null);
        //-------------开始绘制文字-------------------------------

        // 文字开始的坐标,默认为左上角
        float textX = 0;
        float textY = 0;

        if (!TextUtils.isEmpty(markText)) {

            String[] mtarr=markText.split("\r\n");


            // 创建画笔
            Paint mPaint = new Paint();
            // 文字矩阵区域
            Rect textBounds = new Rect();
            // 获取屏幕的密度，用于设置文本大小
            //float scale = context.getResources().getDisplayMetrics().density;
            // 水印的字体大小
            //mPaint.setTextSize((int) (11 * scale));
            mPaint.setTextSize(fontsize);
            // 文字阴影
            mPaint.setShadowLayer(0.5f, 0f, 1f, Color.BLACK);
            // 抗锯齿
            mPaint.setAntiAlias(true);
            // 水印的区域
            mPaint.getTextBounds(markText, 0, markText.length(), textBounds);
            // 水印的颜色
            mPaint.setColor(fontcolor);

            // 当图片大小小于文字水印大小的3倍的时候，不绘制水印
	        /*
	        if (textBounds.width() > bitmapWidth / 3 || textBounds.height() > bitmapHeight / 3) {
	            return bitmap;
	        }
*/
            // 文字开始的坐标
            textY=bitmapHeight-10;//左下角
            // 画文字
            //  canvas.drawText(markText, textX, textY, mPaint);
            for(int i=mtarr.length-1;i>=0;i--)
            {
                //画文字
                canvas.drawText(mtarr[i].toString(), textX, textY-fontsize*(mtarr.length-i-1), mPaint);//左下角

                //Log.v("绘制的水印"+i, mtarr[i].toString());
                //Log.v("坐标X,Y:", textX+","+ (textY-20*(mtarr.length-i-1)));
            }

            //canvas.drawText("这是水印", 100,1000, mPaint);
        }

        //保存所有元素
        canvas.save(Canvas.ALL_SAVE_FLAG);
        canvas.restore();

        return bmp;
    }


    /**
     * 以最省内存的方式读取本地资源的图片
     * @param file
     * @return
     */
    private  Bitmap readBitMap(File file){

        BitmapFactory.Options opt = new BitmapFactory.Options();
        opt.inPreferredConfig = Bitmap.Config.RGB_565;
        opt.inPurgeable = true;
        opt.inInputShareable = true;

        //获取资源图片
        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return BitmapFactory.decodeStream(is,null,opt);
    }


    /*
     * 等比缩放bitmap
     * */
    // 缩放图片
    public static Bitmap zoomImg(Bitmap bm, int newWidth ,int newHeight){
        // 获得图片的宽高
        int width = bm.getWidth();
        int height = bm.getHeight();
        // 计算缩放比例
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // 取得想要缩放的matrix参数
        Matrix matrix = new Matrix();
        //只有当需求大小小于原始大小时进行缩放
        if(scaleWidth<1&&scaleHeight<1)
        {
            //判断图片是竖着的还是横着的
            if(width>=height)
            {
                //横着的
                matrix.postScale(scaleWidth,scaleWidth);

            }else
            {

                matrix.postScale(scaleHeight,scaleHeight);

            }



        }else
        {
            return bm;
        }

        // 得到新的图片
        Bitmap newbm = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, true);
        return newbm;
    }


    private void startThreadUpLoadImage(final File imageFile,
                                        final float fileLength)// 启动线程进行上传图片
    {
        new Thread() {// 启动线程上传
            public void run() {
                //showDialog();
                System.out.println("imageFile = " + imageFile.toString());
                int TIME_OUT = 10 * 1000; // 超时时间
                String CHARSET = "utf-8"; // 设置编码
                String BOUNDARY = UUID.randomUUID().toString(); // 边界标识 随机生成
                String PREFIX = "--", LINE_END = "\r\n";
                String CONTENT_TYPE = "multipart/form-data"; // 内容类型
                System.out.println("httpUrlPath = " + httpUrlPath);
                if (!httpUrlPath.equals("") && httpUrlPath != null) {
                    try {
                        // 获取文件大小
                        URL url = new URL(httpUrlPath);
                        HttpURLConnection conn = (HttpURLConnection) url
                                .openConnection();
                        conn.setReadTimeout(TIME_OUT);
                        conn.setConnectTimeout(TIME_OUT);
                        conn.setDoInput(true); // 允许输入流
                        conn.setDoOutput(true); // 允许输出流
                        conn.setUseCaches(false); // 不允许使用缓存
                        conn.setRequestMethod("POST"); // 请求方式
                        conn.setRequestProperty("Charset", CHARSET); // 设置编码
                        conn.setRequestProperty("connection", "keep-alive");
                        conn.setRequestProperty("Content-Type", CONTENT_TYPE
                                + ";boundary=" + BOUNDARY);
                        conn.connect();
                        if (imageFile != null) {
                            /**
                             * 当文件不为空，把文件包装并且上传
                             */
                            OutputStream outputSteam = conn.getOutputStream();
                            DataOutputStream dos = new DataOutputStream(
                                    outputSteam);
                            StringBuffer sb = new StringBuffer(); // 拼接上传数据
                            sb.append(PREFIX);
                            sb.append(BOUNDARY);
                            sb.append(LINE_END);
                            /**
                             * 这里重点注意： name里面的值为服务器端需要key 只有这个key 才可以得到对应的文件
                             * filename是文件的名字，包含后缀名的 比如:abc.png
                             */
                            sb.append("Content-Disposition: form-data; name=\"file\"; filename=\""
                                    + imageFile.getName() + "\"" + LINE_END);
                            sb.append("Content-Type: image/jpeg; charset="
                                    + CHARSET + LINE_END);
                            sb.append(LINE_END);
                            dos.write(sb.toString().getBytes());

                            InputStream is = new FileInputStream(imageFile);
                            InputStream mProgressInput = new FileInputStream(
                                    imageFile);
                            int count = 0;
                            byte[] bytes = new byte[1024];
                            int len = 0;
                            while ((len = is.read(bytes)) != -1
                                    && isUP_LOAD_IMAGE) {
                                if (isUP_LOAD_IMAGE) {
                                    int numread = mProgressInput.read(bytes);
                                    count += numread;
                                    mProgress = (int) (((float) count / fileLength) * 100);// 计算进度条
                                   // mHandler.sendEmptyMessage(DIALOG_INDEX);
                                    dos.write(bytes, 0, len);
                                } else {
                                    is.close();
                                    mProgressInput.close();
                                    return;
                                }
                            }
                            if (isUP_LOAD_IMAGE) {
                                is.close();
                                mProgressInput.close();
                                dos.write(LINE_END.getBytes());
                                byte[] end_data = (PREFIX + BOUNDARY + PREFIX + LINE_END)
                                        .getBytes();
                                dos.write(end_data);
                                dos.flush();
                                int res = conn.getResponseCode();
                                if (res == 200) {
                                    InputStream input = conn.getInputStream();
                                    BufferedReader b = new BufferedReader(
                                            new InputStreamReader(input,
                                                    "utf-8"));
                                    String ress = b.readLine();
                                    System.out.println("ress  = " + ress);
                                    if (ress != null && !ress.equals("")) {
                                        cancelDialog();// 清楚进度条对话框




                                        //清除压缩后的图片
                                        String temp=imageFile.getName().toUpperCase();
                                        String[] namearr=imageFile.getName().toUpperCase().split("\\.");
                                        if(namearr.length>1)
                                        {
                                            if(namearr[namearr.length-2].toString().equals(namearr[namearr.length-1].toString()))
                                            {
                                                //删除该压缩文件
                                                imageFile.delete();

                                            }
                                        }
                                        showsuccess(ress);
                                    }
                                } else {
                                    cancelDialog();
                                    System.out.println("rescode="+res);
                                    showError("图片上传异常 ，请重新上传:"+res);
                                }
                            }
                        }
                    } catch (Exception e) {
                        cancelDialog();
                        System.out.println("2");
                        showError("图片上传异常 ，请重新上传:"+e.getMessage());
                    }
                } else {
                    cancelDialog();
                    System.out.println("3");
                    showError("图片上传异常 ，请重新上传");
                }
            };
        }.start();
    }

    private void showsuccess(String res)
    {
        //返回成功个结果，通知JS 调用上传
        cbCtx.success(res);
        LOG.v("上传成功后的执行结果", res);
    }
    private void showError(final String error)// 显示错误 通知服务器 异常
    {
        System.out.println("error = " + error);
        new Thread() {
            public void run() {
                cbCtx.error(error);
                System.out.println("error = " + error);
            };
        }.start();
    }

//    @SuppressLint("HandlerLeak")
//    Handler mHandler = new Handler() {
//        public void handleMessage(android.os.Message msg) {
//            switch (msg.what) {
//                case DIALOG_INDEX:
//                    if (mProgress < 100) {
//                        pgDialog.setProgress(mProgress);
//                    }
//                    break;
//                default:
//                    break;
//            }
//        };
//
//    };


    @SuppressWarnings("deprecation")
    public void showDialog()// 显示上传的对话框
    {
        System.out.println("showDilog");
        pgDialog = new ProgressDialog(this.cordova.getActivity());
        pgDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pgDialog.setCancelable(false);
        pgDialog.setProgress(mProgress);
        pgDialog.setTitle("图片正在上传中");
        pgDialog.setMessage("请稍后。。。");
        pgDialog.setMax(100);
        pgDialog.setButton("取消上传", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                isUP_LOAD_IMAGE = false;
                pgDialog.cancel();
            }
        });
        pgDialog.show();
    }



    public void cancelDialog() // 关闭上传的对话框
    {
        System.out.println("cancelDialog");
        if (pgDialog != null && pgDialog.isShowing()) {
            pgDialog.cancel();
        }
    }

}
