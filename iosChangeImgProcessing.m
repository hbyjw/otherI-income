//说明：如果今年你们没有改过这个方法 ，直接覆盖就好了
//如果改过这个方法，那么代码里面有注释，那些事修改了的

-(void)ImgProcessing:(CDVInvokedUrlCommand*)command
{
    NSError  *error;
    NSString *jsonstr=[command.arguments objectAtIndex:0];
    NSData *jsonData = [jsonstr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    self.mHttpURL=[jsondic objectForKey:@"url"];
    NSString *zoom=[jsondic objectForKey:@"zoom"];//缩放
    NSArray * zoomdic = [zoom componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    
    NSString *nw=[zoomdic objectAtIndex:0];
    NSString *nh=[zoomdic objectAtIndex:1];
    
    NSString *watermark=[jsondic objectForKey:@"watermark"];//水印
    NSArray  *watermarkdic = [watermark componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    
    self.wmflg=[[watermarkdic objectAtIndex:0] intValue];
    self.watermarkfontsize=[[watermarkdic objectAtIndex:1] intValue];
    self.watermarkfontcolor=[watermarkdic objectAtIndex:2];
    self.watermarktext=[watermarkdic objectAtIndex:3];
    
    self.needwidth=[nw intValue];
    self.needheight=[nh intValue];
    NSString *imgpath=[jsondic objectForKey:@"imgpath"];

    //imgpath=@"/private/var/mobile/Containers/Data/Application/3ACAF15A-4876-40C7-9306-09CCC0E1F94A/tmp/cdv_photo_011.jpg";
    NSString *tmpDir = NSTemporaryDirectory();
    NSArray *aArray = [imgpath componentsSeparatedByString:@"/"];
    NSString *filename=[aArray lastObject];
    NSString *newpath=[tmpDir stringByAppendingString:filename];
    UIImage *img=[[UIImage alloc] initWithContentsOfFile:newpath];
    
    /*新增代码 20180728   0*/
     NSString *groupno=[jsondic objectForKey:@"groupno"];
    /*新增代码 20180728  1*/
    
    
    //表示需要进行缩放
    if(self.needwidth>0)
    {
        img = [self scaleToSize:img size:CGSizeMake(0,0)];
        
        if(wmflg==1)//需要添加水印
        {
            NSLog(@"1水印文字为:%@",self.watermarktext);
            
            img=  [self watermarkImage:img withWaterMark:self.watermarktext wmfontsize:(int)watermarkfontsize ];
            
        }
        
    }else
    {
        //不缩放
        if(wmflg==1) //需要添加水印
        {
            NSLog(@"2水印文字为:%@",self.watermarktext);
            img=  [self watermarkImage:img withWaterMark:self.watermarktext wmfontsize:(int)watermarkfontsize ];
            
        }
    }
    //将处理后的图片保存到临时目录
    //压缩上传
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.3f;
    NSData *imageData = UIImageJPEGRepresentation(img, 0.7);
  
    while ([imageData length] > 500 && compression > maxCompression) {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(img, compression);
    }
    
/*
        //这里面是原来的代码，下面其实只是加了个判断
        NSString *_encodedImageStr = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:_encodedImageStr];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
*/
       /*修改代码 20180728   0*/
    if([groupno isEqualToString:@""])
    {
        //保存到app存储空间
        [imageData writeToFile:newpath atomically:NO];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:imgpath];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }else
    {
        //该代码段为原来的
        //保存到IndexDB
        NSString *_encodedImageStr = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:_encodedImageStr];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    
       /*修改代码 20180728   1*/
}