//
//  ImgProcessing.m
//  智企互联
//
//  Created by feixun on 2017/10/31.
//
//

#import "ImgProcessing.h"
@implementation ImgProcessing
@synthesize wmflg;//是否需要添加水印
@synthesize watermarkfontsize;//水印文字大小
@synthesize watermarkfontcolor;//水印颜色
@synthesize watermarktext;//水印文字

-(void)ImgProcessing:(CDVInvokedUrlCommand*)command//调用系统相册
{
    NSError  *error;
    NSString *jsonstr=[command.arguments objectAtIndex:0];
    NSData *jsonData = [jsonstr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    self.mHttpURL=[jsondic objectForKey:@"url"];
    NSString *zoom=[jsondic objectForKey:@"zoom"];//缩放
    NSArray * zoomdic = [zoom componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    
    NSString *nw=[zoomdic objectAtIndex:0];
    NSString *nh=[zoomdic objectAtIndex:1];
    
    NSString *watermark=[jsondic objectForKey:@"watermark"];//水印
    NSArray  *watermarkdic = [watermark componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    
    self.wmflg=[[watermarkdic objectAtIndex:0] intValue];
    self.watermarkfontsize=[[watermarkdic objectAtIndex:1] intValue];
    self.watermarkfontcolor=[watermarkdic objectAtIndex:2];
    self.watermarktext=[watermarkdic objectAtIndex:3];
    
    self.needwidth=[nw intValue];
    self.needheight=[nh intValue];
    NSString *imgpath=[jsondic objectForKey:@"imgpath"];

    //imgpath=@"/private/var/mobile/Containers/Data/Application/3ACAF15A-4876-40C7-9306-09CCC0E1F94A/tmp/cdv_photo_011.jpg";
    NSString *tmpDir = NSTemporaryDirectory();
    NSArray *aArray = [imgpath componentsSeparatedByString:@"/"];
    NSString *filename=[aArray lastObject];
    NSString *newpath=[tmpDir stringByAppendingString:filename];
    UIImage *img=[[UIImage alloc] initWithContentsOfFile:newpath];
    
    /*新增代码 20180728   0*/
     NSString *groupno=[jsondic objectForKey:@"groupno"];
      /*新增代码 20180728  1*/
    
    
    //表示需要进行缩放
    if(self.needwidth>0)
    {
        img = [self scaleToSize:img size:CGSizeMake(0,0)];
        
        if(wmflg==1)//需要添加水印
        {
            NSLog(@"1水印文字为:%@",self.watermarktext);
            
            img=  [self watermarkImage:img withWaterMark:self.watermarktext wmfontsize:(int)watermarkfontsize ];
            
        }
        
    }else
    {
        //不缩放
        if(wmflg==1) //需要添加水印
        {
            NSLog(@"2水印文字为:%@",self.watermarktext);
            img=  [self watermarkImage:img withWaterMark:self.watermarktext wmfontsize:(int)watermarkfontsize ];
            
        }
    }
    //将处理后的图片保存到临时目录
    //压缩上传
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.3f;
    NSData *imageData = UIImageJPEGRepresentation(img, 0.7);
  
    while ([imageData length] > 500 && compression > maxCompression) {
        compression -= 0.1;
        imageData = UIImageJPEGRepresentation(img, compression);
    }
    
       /*修改代码 20180728   0*/
    if([groupno isEqualToString:@""])
    {
        //保存到app存储空间
        [imageData writeToFile:newpath atomically:NO];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:imgpath];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }else
    {
        //该代码段为原来的
        //保存到IndexDB
        NSString *_encodedImageStr = [imageData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithLineFeed];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:_encodedImageStr];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
    
       /*修改代码 20180728   1*/
}



-(void)mosaicUrl//拼接url 获取缩略分辨率大小
{
    
    NSError  *error;
    NSString *jsonstr=[self.command.arguments objectAtIndex:0];
    NSData *jsonData = [jsonstr dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsondic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&error];
    
    self.mHttpURL=[jsondic objectForKey:@"url"];
    NSString *zoom=[jsondic objectForKey:@"zoom"];//缩放
    NSArray * zoomdic = [zoom componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    
    NSString *nw=[zoomdic objectAtIndex:0];
    NSString *nh=[zoomdic objectAtIndex:1];
    
    NSString *watermark=[jsondic objectForKey:@"watermark"];//水印
    NSArray  *watermarkdic = [watermark componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@","]];
    
    self.wmflg=[[watermarkdic objectAtIndex:0] intValue];
    self.watermarkfontsize=[[watermarkdic objectAtIndex:1] intValue];
    self.watermarkfontcolor=[watermarkdic objectAtIndex:2];
    self.watermarktext=[watermarkdic objectAtIndex:3];
    
    self.needwidth=[nw intValue];
    self.needheight=[nh intValue];
    
    
}

-(void)error{
   // [MBProgressHUD hideHUDForView:self.viewController.navigationController.view animated:YES];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"error"];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
}

- (void)upload:(CDVInvokedUrlCommand *)command{//浏览按钮
    
    //    NSString *sourceFile =[[NSBundle mainBundle] pathForResource:@"test.db" ofType:nil];
    //    NSString* resourcePath = [[NSBundle mainBundle] resourcePath];
    //    [resourcePath appendString:@"myapp.db"];
    //    Boolean ife=[[NSFileManager defaultManager] fileExistsAtPath:resourcePath];
    //    if (![[NSFileManager defaultManager] fileExistsAtPath:resourcePath])
    //    {
    //
    //
    //    }
    
    self.command =command;
    [self mosaicUrl];
    if (self.mHttpURL != nil) {
        NSLog(@"mHttpURL = %@",self.mHttpURL);
        UIAlertView * alertView = [[UIAlertView alloc] initWithTitle:@"上传图片" message:@"请选择图片上传" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"相册",@"相机", nil];
        [alertView show];
    }else{
        [self error];
    }
    //    UploadPhotoViewController *controller = [[UploadPhotoViewController alloc] init];
    //   // controller.delegate = self;
    //    [self.viewController.navigationController setNavigationBarHidden:NO animated:NO];
    //
    //
    //
    //    // 设置背景颜色为。
    //    self.viewController.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.0 green:160/255.0 blue:250/255.0 alpha:1];
    //    [self.viewController.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor],UITextAttributeTextColor,nil]];
    //
    //    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    //
    //    [self.viewController.navigationController pushViewController:controller animated:YES];
}


-(void)camera:(CDVInvokedUrlCommand*)command//调用系统相机
{
    self.command = command;
    [self mosaicUrl];
    if (self.mHttpURL != nil) {
        NSLog(@"mHttpURL = %@",self.mHttpURL);
        [self cameraImage];
    }else{
        [self error];
    }
    
}
-(void)pick:(CDVInvokedUrlCommand*)command//调用系统相册
{
    self.command = command;
    [self mosaicUrl];
    if (self.mHttpURL != nil) {
        NSLog(@"mHttpURL = %@",self.mHttpURL);
        [self pickImage];
    }else
    {
        [self error];
    }
    
}

//获取系统返回图片
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    /*
    NSLog(@"imagePickerController");
    HUD = [MBProgressHUD showHUDAddedTo:self.viewController.navigationController.view animated:YES];
    HUD.dimBackground =YES;
    HUD.delegate =self;
    HUD.labelText =@"请稍等";
    HUD.square =YES;
    [HUD showWhileExecuting:@selector(upLoadImage:)onTarget:self withObject:info animated:YES];
    //SETIMAGE([info objectForKey:@"UIImagePickerControllerOriginalImage"]);
    //    [self dismissModalViewControllerAnimated:YES];
    [self.viewController.navigationController dismissViewControllerAnimated:YES completion:nil];
    [picker release];
    */
}
//相机点cancel时候的调用
-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    /*
    [self.viewController.navigationController dismissViewControllerAnimated:YES completion:nil];
    [picker release];
    NSLog(@"imagePickerControllerDidCancel");
    [self error];
    */
    
}



//上传
-(void)upLoadImage:(NSDictionary*)info
{
    /*
    if (self.mHttpURL!=nil) {
        NSLog(@"upLoadImage");
        NSLog(@" mHttpURL = %@" ,self.mHttpURL);
        NSURL *URL = [NSURL URLWithString:self.mHttpURL];
        ASIFormDataRequest *Request = [ASIFormDataRequest requestWithURL:URL];
        
        [Request setRequestMethod:@"POST"];
        [Request addRequestHeader:@"Content-Type" value:@"image/jpg"];
        [Request setTimeOutSeconds:60];
        //接受图片
        UIImage *img =[info objectForKey:@"UIImagePickerControllerOriginalImage"];
        //表示需要进行缩放
        if(self.needwidth>0)
        {
            img = [self scaleToSize:img size:CGSizeMake(0,0)];
            
            if(wmflg==1)//需要添加水印
            {
                NSLog(@"1水印文字为:%@",self.watermarktext);
                
                img=  [self watermarkImage:img withWaterMark:self.watermarktext wmfontsize:(int)watermarkfontsize ];
                
            }
            
        }else
        {
            //不缩放
            if(wmflg==1) //需要添加水印
            {
                NSLog(@"2水印文字为:%@",self.watermarktext);
                img=  [self watermarkImage:img withWaterMark:self.watermarktext wmfontsize:(int)watermarkfontsize ];
                
            }
        }
        
        //压缩上传
        CGFloat compression = 0.9f;
        CGFloat maxCompression = 0.3f;
        NSData *imageData = UIImageJPEGRepresentation(img, 0.7);
        while ([imageData length] > 500 && compression > maxCompression) {
            compression -= 0.1;
            imageData = UIImageJPEGRepresentation(img, compression);
        }
        
        [Request setData:imageData withFileName:@"myphoto.jpg" andContentType:@"image/jpg" forKey:@"file"];
        
        [Request setDelegate:self];
        
        [Request setCompletionBlock:^{
            NSString *responseString = [Request responseString];
            NSLog(@"Response: %@", responseString);
           // [MBProgressHUD hideHUDForView:self.viewController.navigationController.view animated:YES];
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:responseString];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
        }];
        [Request setFailedBlock:^{
            NSError *error = [Request error];
            NSLog(@"Error: %@,%@", error.localizedDescription,Request.url);
            //[MBProgressHUD hideHUDForView:self.viewController.navigationController.view animated:YES];
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.localizedDescription];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:self.command.callbackId];
            
        }];
        [Request startSynchronous];
    }else{
        [self error];
    }
     */
}

//给图片添加水印
-(UIImage *)watermarkImage:(UIImage *)img withWaterMark:(NSString *)text wmfontsize:(int)fs
{
    
    NSLog(@"3水印文字为:%@",self.watermarktext);
    
    NSString* mark = text;
    
    NSArray *markarr = [mark componentsSeparatedByString:@"\r\n"];
    
    int macount=(int)[markarr count] ;
    
    int w = img.size.width;
    
    int h = img.size.height;
    
    UIGraphicsBeginImageContext(img.size);
    
    [img drawInRect:CGRectMake(0, 0, w, h)];
    
    UIColor *wmfc=nil;
    NSArray *colorarr=@[@"BLACK",@"DKGRAY",@"GRAY",@"LTGRAY",@"WHITE",@"RED",@"GREEN",@"BLUE",@"YELLOW",@"CYAN",@"MAGENTA"];
    int carrid=(int)[colorarr indexOfObject:[watermarkfontcolor uppercaseString]];
    switch (carrid) {
        case 0:
            wmfc=[UIColor blackColor];
            break;
        case 1:
            wmfc=[UIColor darkGrayColor];
            break;
        case 2:
            wmfc=[UIColor grayColor];
            break;
        case 3:
            wmfc=[UIColor lightGrayColor];
            break;
        case 4:
            wmfc=[UIColor whiteColor];
            break;
        case 5:
            wmfc=[UIColor redColor];
            break;
        case 6:
            wmfc=[UIColor greenColor];
            break;
        case 7:
            wmfc=[UIColor blueColor];
            break;
        case 8:
            wmfc=[UIColor yellowColor];
            break;
        case 9:
            wmfc=[UIColor cyanColor];
            break;
        case 10:
            wmfc=[UIColor magentaColor];
            break;
        default:
            wmfc=[UIColor redColor];
    }
    NSDictionary *attr = @{
                           NSFontAttributeName: [UIFont boldSystemFontOfSize:fs],   //设置字体
                           NSForegroundColorAttributeName : wmfc     //设置字体颜色
                           };
    
    
    //[mark drawInRect:CGRectMake(0, 10, w, h) withAttributes:attr];                 //左上角
    
    // [mark drawInRect:CGRectMake(w - 80, 10, 80, 32) withAttributes:attr];            //右上角
    
    //[mark drawInRect:CGRectMake(w - 80, h - 32 - 10, 80, 32) withAttributes:attr];   //右下角
    
    [mark drawInRect:CGRectMake(10, h-((macount+1)*fs), w, h) withAttributes:attr];        //左下角
    
    UIImage *aimg = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return aimg;
    
}


//对话框 UIAlertViewDelegate
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            NSLog(@"取消");
            break;
        case 1:
            NSLog(@"相册");
            [self pickImage];
            break;
        case 2:
            NSLog(@"相机");
            [self cameraImage];
            break;
        default:
            break;
    }
}


- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
    // 创建一个bitmap的context
    
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.3f;
    
    
    // NSLog(@"压缩前的大小为:%d",imageData.length);
    
    //获取到原图的像素判断是否需要缩放
    
    CGSize myImageSize = img.size;
    float scaleWidth=self.needwidth/myImageSize.width;
    float scaleHeight=self.needheight/myImageSize.height;
    
    if(scaleWidth<1&&scaleHeight<1)
    {
        //判断图片是竖着的还是横着的
        if(myImageSize.width>=myImageSize.height)
        {
            //横着的
            size.width=self.needwidth;
            size.height=myImageSize.height*scaleWidth;
            
            
        }else
        {
            size.height=self.needheight;
            size.width=scaleHeight*myImageSize.width;
        }
        
        // 创建一个bitmap的context
        // 并把它设置成为当前正在使用的context
        UIGraphicsBeginImageContext(size);
        // 绘制改变大小的图片
        [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
        // 从当前context中创建一个改变大小后的图片
        UIImage* img = UIGraphicsGetImageFromCurrentImageContext();
        // 使当前的context出堆栈
        UIGraphicsEndImageContext();
        // 返回新的改变大小后的图片
        
        /*
         
         NSData *imageData = UIImageJPEGRepresentation(img, compression);
         while ([imageData length] > 500 && compression > maxCompression) {
         compression -= 0.1;
         imageData = UIImageJPEGRepresentation(img, compression);
         }
         */
        return  img;
        
        
    }else
    {
        
        /*
         NSData *imageData = UIImageJPEGRepresentation(img, compression);
         while ([imageData length] > 500 && compression > maxCompression) {
         compression -= 0.1;
         imageData = UIImageJPEGRepresentation(img, compression);
         }
         */
        return img;
        
    }
    
    
    
    //    //是否压缩
    //    if(1==1)
    //    {
    //        while ([imageData length] > 1500 && compression > maxCompression) {
    //            compression -= 0.1;
    //            imageData = UIImageJPEGRepresentation(img, compression);
    //        }
    //
    //    }
    //
    //    UIImage *compressedImage = [UIImage imageWithData:imageData];
    //
    //
    //    return compressedImage;
}
//- (UIImage *)scaleToSize:(UIImage *)img size:(CGSize)size{
//    // 创建一个bitmap的context
//    // 并把它设置成为当前正在使用的context
//    UIGraphicsBeginImageContext(size);
//
//   // NSLog(@"图片大小%@",img.self.size) ;
//    // 绘制改变大小的图片
//    [img drawInRect:CGRectMake(0,0, size.width, size.height)];
//    // 从当前context中创建一个改变大小后的图片
//    UIImage* scaledImage =UIGraphicsGetImageFromCurrentImageContext();
//    // 使当前的context出堆栈
//    UIGraphicsEndImageContext();
//    //返回新的改变大小后的图片
//    return scaledImage;
//}
//camera
-(void)cameraImage
{
    /*
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusDenied){//打开相机 就调用相机
        UIAlertView * alertView = [[UIAlertView alloc]
                                   initWithTitle:@"相机异常"
                                   message:@"启动相机异常,请检查隐私中是否允许该应用使用相机."
                                   delegate:nil
                                   cancelButtonTitle:@"确定" otherButtonTitles:nil];
        [alertView show];
    }else{
        UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        ipc.delegate =self;
        ipc.allowsEditing =NO;
        //[self presentModalViewController:ipc animated:YES];
        [self.viewController presentModalViewController:ipc animated:YES];
    }
    */
}
//获取相册
- (void) pickImage
{
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.delegate =self;
    ipc.allowsEditing =NO;
    //[self presentModalViewController:ipc animated:YES];
    [self.viewController presentModalViewController:ipc animated:YES];
}


@end
