//以下js为新添加的，你可以直接附加在feixun.js后面或者新建一个js文件，然后引用


var dbInfo = {
    name: 'SaveImages',
    version: 1,
    db: null,
    ojstore: {
        name: 'images',//存储空间表的名字
        keypath: 'timestamp'//主键
    }
};

var myIndexedDB = {
    indexedDB: window.indexedDB || window.webkitindexedDB,
    IDBKeyRange: window.IDBKeyRange || window.webkitIDBKeyRange,//键范围
    openDB: function (dbname, dbversion, callback) {
        //建立或打开数据库，建立对象存储空间(ObjectStore)
        var self = this;
        var version = dbversion || 1;

        var request = self.indexedDB.open(dbname, version);
        request.onerror = function (e) {
            // callback(false)
            console.log(e.currentTarget.error.message);
        };
        request.onsuccess = function (e) {
            // callback(true)
            dbInfo.db = e.target.result;

            console.log('成功建立并打开数据库:' + dbInfo.name + ' version' + dbversion);
        };
        request.onupgradeneeded = function (e) {
            var db = e.target.result, transaction = e.target.transaction, store;
            if (!db.objectStoreNames.contains(dbInfo.ojstore.name)) {
                //没有该对象空间时创建该对象空间
                store = db.createObjectStore(dbInfo.ojstore.name, { keyPath: dbInfo.ojstore.keypath });
                store.createIndex('groupnoIndex', 'groupno', { unique: false });
                console.log('成功建立对象存储空间：' + dbInfo.ojstore.name);
            }
        }


    },
    deletedb: function (dbname) {
        //删除数据库
        var self = this;
        self.indexedDB.deleteDatabase(dbname);
        console.log(dbname + '数据库已删除')
    },
    closeDB: function (db) {
        //关闭数据库
        db.close();
        console.log('数据库已关闭')
    },
    addData: function (db, storename, data, callback) {
        //添加数据，重复添加会报错
        var store = store = db.transaction(storename, 'readwrite').objectStore(storename), request;
        for (var i = 0; i < data.length; i++) {
            request = store.add(data[i]);
            request.onerror = function () {
                callback(false)
                console.log('add添加数据库中已有该数据')
            };
            request.onsuccess = function () {

                callback(true)
                console.log('add添加数据已存入数据库')
            };
        }



    },
    putData: function (db, storename, data) {
        //添加数据，重复添加会更新原有数据
        var store = store = db.transaction(storename, 'readwrite').objectStore(storename), request;
        for (var i = 0; i < data.length; i++) {
            request = store.put(data[i]);
            request.onerror = function () {
                console.error('put添加数据库中已有该数据')
            };
            request.onsuccess = function () {
                console.log('put添加数据已存入数据库')
            };
        }
    },
    getDataByKey: function (db, storename, key, callback) {

        //根据存储空间的键找到对应数据
        var store = db.transaction(storename, 'readwrite').objectStore(storename);
        var request = store.get(key);
        request.onerror = function () {
            console.error('getDataByKey error');
        };
        request.onsuccess = function (e) {
            var result = e.target.result;
            callback(result)
            console.log('查找数据成功')
            console.log(result);
        };
    },
    getMultipleData(db, storeName, groupno, callback) {
        //alert(groupno)
        var transaction = db.transaction(storeName);
        var store = transaction.objectStore(storeName);
        var index = store.index("groupnoIndex");
        var resdata = []
        var request = index.openCursor(IDBKeyRange.only(groupno))
        request.onsuccess = function (e) {
            var cursor = e.target.result;

            if (cursor) {

                resdata.push(cursor.value)
                console.log(cursor);
                cursor.continue();
            } else {
                console.log("resdata", JSON.stringify(resdata));
                callback(resdata)
            }
        }
    },

    getDataAll: function (db, storename, callback) {
        //根据存储空间的键找到对应数据
        var store = db.transaction(storename, 'readwrite').objectStore(storename);
        var request = store.getAll();
        request.onerror = function () {
            alert('getDataByKey error');
        };
        request.onsuccess = function (e) {
            var result = e.target.result;
            callback(result)
            //alert('查找数据成功')
            console.log(result);
        };
    },
    deleteData: function (db, storename, key, callback) {
        try {
            //删除某一条记录
            var store = store = db.transaction(storename, 'readwrite').objectStore(storename);
            store.delete(key)
            console.log('已删除存储空间' + storename + '中' + key + '记录');
            callback({ "success": true })
        } catch (err) {
            callback({ "success": false, "msg": err })
        }

    },
    clearData: function (db, storename) {
        //删除存储空间全部记录
        var store = db.transaction(storename, 'readwrite').objectStore(storename);
        store.clear();
        console.log('已删除存储空间' + storename + '全部记录');
    }
}

myIndexedDB.openDB(dbInfo.name, dbInfo.version);

//获取指定分组的图片
//取得图片信息数组后的回调方法
function selectImages(groupno, callback) {
    try {
        myIndexedDB.getMultipleData(dbInfo.db, dbInfo.ojstore.name, groupno, callback)
        //  myIndexedDB.getMultipleData(dbInfo.db,dbInfo.ojstore.name,groupno,(data)=>{
        //  console.log("callback",data)
        //  })
    } catch (err) {
        alert(err)
    }
}

//图片字符串，
//分组编号
function insertImage(data, groupno,funstr) {
    // alert(groupno)
    var imageobj = [{
        timestamp: Date.parse(new Date()),
        groupno: groupno,
        imgstr: data
    }]
    myIndexedDB.addData(dbInfo.db, dbInfo.ojstore.name, imageobj, (res) => {
        if (res) {
            eval(funstr)
            alert("保存成功!")
        }
    });
}


//按分组删除图片
//groupno 为分组号
function deleteImageGroupno(groupno) {

    try {
        myIndexedDB.getMultipleData(dbInfo.db, dbInfo.ojstore.name, groupno, (data) => {
            for (var i = 0; i < data.length; i++) {
                myIndexedDB.deleteData(dbInfo.db, dbInfo.ojstore.name, data[i].timestamp, (res) => {
                    console.log(res)
                })
            }

        })
    } catch (err) {
        alert(err)
    }

}





//从indexdb读取图片上传
//timestamp 保存在indexdb中的主键值
function uploadImageFormIndexDb(timestamp, callback) {
    myIndexedDB.getDataByKey(dbInfo.db, dbInfo.ojstore.name, timestamp, (res) => {

        var base64str = res.imgstr
        var userno = localStorage.userno;//获取用户编号
        var formid = getformid();
        var data = {
            userno: userno,
            formid: formid,
            imgdata: "data:image/jpeg;base64," + base64str,
            appno: localStorage.getItem("appno")
        }
        data = JSON.stringify(data)
        data = JSON.parse(data)
        $.ajax({
            dataType: "json",
            data: data,
            timeout: localStorage.timeout,
            type: "post",
            url: localStorage.serveraddress + "/ashx/app/base64upload.ashx",
            success: function (res) {

                callback(res)

                console.log(res)
            },
            error: function (err) {
               
                try{
                    
                    alert(JSON.stringify(err))
                }catch(error) 
                {
                     alert(err+"|" +error)
                }
                
                console.log(err)
            }
        })
    })

}
