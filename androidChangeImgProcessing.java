

//说明：如果今年你们没有改过这个方法 ，直接覆盖就好了
//如果改过这个方法，那么代码里面有注释，那些事修改了的
//注意：新添加了一个方法 imageToBase64


    //需要添加一下变量
    private String groupno="";//本地保存图片的分组号 ""表示不需要本地保存

    //需要在 public boolean execute(....) 中添加一下变量取值
    if(myJsonObject.has("groupno"))
    {
        groupno=myJsonObject.getString("groupno");
    }

   //被修改的方法
    private void startThreadCompressImage(final File imageFile,final String uploadPhotPath ,final float fileLength)
    {
        new Thread()
        {
            public void run()
            {
                //上传前对图片进行压缩
                Bitmap bitmap=readBitMap(imageFile);
                File newfile=new File(uploadPhotPath);//将要保存图片的路径
                if(needheight>0)
                {
                    //对图片进行等比缩放
                    bitmap=	zoomImg(bitmap,needheight,needwidth);
                }

                //  bitmap=Watermarkbmp;
                //判断是否需要添加水印
                if(wmflg==1)
                {
                    //压缩后的图片添加水印
                    wmfc=wmfc.toUpperCase();
                    int fci=0xFFFF0000;//默认黑色
                    //获取文字颜色
                    if(wmfc.equals("BLACK"))
                    {
                        fci=0xFF000000;
                    }else if (wmfc.equals("DKGRAY"))
                    {
                        fci=0xFF444444;
                    }else if (wmfc.equals("GRAY"))
                    {
                        fci=0xFF888888;
                    }else if (wmfc.equals("LTGRAY"))
                    {
                        fci=0xFFCCCCCC;
                    }else if (wmfc.equals("WHITE"))
                    {
                        fci=0xFFFFFFFF;
                    }else if (wmfc.equals("RED"))
                    {
                        fci=0xFFFF0000;
                    }else if (wmfc.equals("GREEN"))
                    {
                        fci=0xFF00FF00;
                    }else if (wmfc.equals("BLUE"))
                    {
                        fci=0xFF0000FF;
                    }else if (wmfc.equals("YELLOW"))
                    {
                        fci=0xFFFFFF00;
                    }else if (wmfc.equals("CYAN"))
                    {
                        fci=0xFF00FFFF;
                    }else if (wmfc.equals("MAGENTA"))
                    {
                        fci=0xFFFF00FF;
                    }else if (wmfc.equals("TRANSPARENT"))
                    {
                        fci=0;
                    }
                    /**
                     * 添加水印
                     *
                     * @param bitmap       原图
                     * @param markText     水印文字
                     * @param fontsize     文字大小
                     * @param fontcolor    文字颜色
                     * @return bitmap      打了水印的图
                     */
                    bitmap=createWatermark(bitmap,wmtxt,wmfz,fci);
                }
                //这里面开始进行图片压缩
                try {
                    FileOutputStream out=new FileOutputStream(newfile);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, baos);
                    int options = 80;
                    while (baos.toByteArray().length > 1024 * 1024) { // 循环判断如果压缩后图片是否大于1000kb,大于继续压缩
                        LOG.v("baos.toByteArray.length", baos.toByteArray().length + "");
                        baos.reset(); // 重置baos即清空baos
                        bitmap.compress(Bitmap.CompressFormat.JPEG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
                        if(options>=30)
                        {
                            options -= 10;// 每次都减少10
                        }else
                        {
                            bitmap.compress(Bitmap.CompressFormat.JPEG, options, baos);
                            break;
                        }
                    }
                    out.write(baos.toByteArray());
                    out.flush();
                    out.close();


                    //根据情况判断是否需要,如果是详细拍照的那么删除，如果是相册选择的
                    imageFile.delete();
                    
                    /*
                        //这里是原来的方法，新方法只是添加了一个判断
                         showsuccess(uploadPhotPath);
                    */


                      /*修改代码 20180729   0*/
                    if(groupno.equals(""))
                    {
                        //返回压缩后的图片路径，然后调用文件上传插件进行上传
                        showsuccess(uploadPhotPath);

                    }else
                    {

                            showsuccess(imageToBase64(uploadPhotPath));
                    }
                 /*修改代码 20180729   1*/

                 //   startThreadUpLoadImage(newfile, fileLength);

                } catch (IOException e) {
                    e.printStackTrace();
                    //压缩失败已原图方式上传
                    showsuccess(imageFile.getAbsolutePath());
                    //startThreadUpLoadImage(imageFile, fileLength);
                }


            }
        }.start();
    }
    /**
     * 将图片转换成Base64编码的字符串
     * @param path
     * @return base64编码的字符串
     */
    public static String imageToBase64(String path) {
        if (TextUtils.isEmpty(path)) {
            return null;
        }
        InputStream is = null;
        byte[] data = null;
        String result = null;
        try {
            is = new FileInputStream(path);
            //创建一个字符流大小的数组。
            data = new byte[is.available()];
            //写入数组
            is.read(data);
            //用默认的编码格式进行编码
            result = Base64.encodeToString(data, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != is) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
        Log.v("文件长度", String.valueOf(result.length()));
        return result;
    }