//以下为修改的js，源js方法直接来源于你们的App安装包

//注意:新添加了2个参数
//第9个参数为保存在本地的分组号，要求字符串
//第10个参数为文件来源选择  0 文件 1 相机 其他值为用户自己选择来源

//  localStorage.internetaddress
//选择图片上传
//showimgid  图片在界面上显示的src的id   ,imgdid 返回文档编号保存在界面上的input 控件的ID,funstr上传成功之后要执行的方法 如果传入''表示不执行
//funstr 是在获取到图片在数据库中的did后 要执行的方法
//如果只是添加水印，不需要修改图片像素,将第4，5个参数写成 9999,9999,水印文字中有换行，请添加 \r\n
//添加水印需要3个参数 1、字体大小 2、字体颜色 3、水印文字内容
//uploadfilesa("showid","imgdid","'doclick(\'1009-1\',\'did\',\'其他参数\')'") 其中did固定不可以修改,但是可以修改所在位置
function uploadfilesa(showimgid, imgdid, funstr) {

    var needheight = 0;
    var needwidth = 0;
    var wmflg = 0;//表示不需要添加水印
    var wmfontsize = 20;//水印文字大小
    var wmfontclolr = "red";//水印文字颜色
    var wmtxt = " ";//要添加的水印文字
    /*添加的 20180728  start*/
    var groupno = "";//保存到indexdb的分组号
    var imageSourceType = 9;//选择图片来源,0相册，1相机
    /*添加的 20180728  end*/
    if (arguments.length > 3) {
        //表示有设置长宽缩放
        needwidth = arguments[3];
        needheight = arguments[4];
    }
    if (arguments.length > 5) {
        wmflg = 1;//表示需要添加水印
        //表示要添加水印，如果只是添加水印，不要修改图片像素,将第4，5个参数写成 9999,9999
        wmfontsize = arguments[5];
        wmfontclolr = arguments[6];
        wmtxt = arguments[7];
    }
    /*添加的 20180728  start*/
    if (arguments.length > 8) {
        // alert("保存在indexdb")
        groupno = arguments[8];
        // alert(groupno)

    }

    if (arguments.length > 9) {

        imageSourceType = arguments[9];

    }
    /*添加的 20180728  end*/
    //alert(wmflg)
    var userno = localStorage.userno;//获取用户编号
    var formid = getformid();
    var url = localStorage.serveraddress + "/ajaxfileupload.ashx?userno=" + userno + "&formid=" + formid + "";
    var data = JSON.stringify({ "zoom": needwidth + "," + needheight, "url": url, "watermark": "" + wmflg + "," + wmfontsize + "," + wmfontclolr + "," + wmtxt + "" });
    var jsonstr;
    if (localStorage.ios == 0 || localStorage.ios == 1) {
        console.log(01)
        /*添加的 20180728  start*/
        var btnarr
        if (imageSourceType == 0) {
            btnarr = ["相册", "取消"]
        } else if (imageSourceType == 1) {
            btnarr = ["相机", "取消"]
        } else {
            btnarr = ["相册", "相机", "取消"]
        }
        /*添加的 20180728  end*/

        var sourceType = 0;//选择图片来源 0 文件 1 相机
        // confirm: function(message, resultCallback, title, buttonLabels)
        navigator.notification.confirm("选择文件来源", function (res) {


            if (imageSourceType == 0) {
                if (res == 1) {
                    sourceType = 0;
                } else if (res == 0 || res == 2) {
                    return;
                }
            } else if (imageSourceType == 1) {
                if (res == 1) {
                    sourceType = 1;
                } else if (res == 0 || res == 2) {
                    return;
                }
            } else {
                if (res == 2) {
                    sourceType = 1;
                } else if (res == 0 || res == 3) {
                    return;
                }
            }

            //调用插件进行拍照
            navigator.camera.getPicture(
                function (success) {
                    if (localStorage.ios == 1) {
                        // $.afui.showMask("正在处理图片...");
                    } else {
                        navigator.notification.activityStart("提示", "正在处理图片...");
                    }

                    // toast.showLong("正在处理图片...",null,null);
                    // alert("'3123");
                    // alert(success);
                    console.log("拍照保存的图片路径:" + success);
                    // var data=JSON.stringify({"imgpath":success, "zoom":needwidth+","+needheight,"url":url,"watermark":""+wmflg+","+wmfontsize+","+wmfontclolr+","+wmtxt+""});
                    var data = JSON.stringify({ "imgpath": success, "zoom": needwidth + "," + needheight, "url": url, "watermark": "" + wmflg + "," + wmfontsize + "," + wmfontclolr + "," + wmtxt + "", "groupno": groupno });
                    feixun.imgproc.ImgProcessing(

                        function (success) {
                            //alert(success);


                            console.log("处理图片成功!" + success);
                            if (groupno != "") {
                                insertImage(success, groupno,funstr)
                                return
                            }

                            //  toast.cancel(null,null);
                            //  $.afui.hideMask();
                            //图片处理成功 调用上传插件上传图片
                            //FileTransfer.prototype.upload = function(filePath, server, successCallback, errorCallback, options, trustAllHosts)
                            var options = new FileUploadOptions();
                            options.fileKey = "file";
                            options.fileName = success.substr(success.lastIndexOf('/') + 1);
                            //  options.mimeType = "text/html";
                            var params = {};
                            params.value1 = "delete";//表示上传完之后需要删除
                            // params.value2 = "param";
                            options.params = params;
                            //  alert("处理后的图片路径:"+success)
                            var ft = new FileTransfer();
                            //alert("1")
                            if (localStorage.ios == "0") {
                                navigator.notification.progressStart("正在上传", "当前进度");
                                //    alert("2")
                                navigator.notification.activityStop();
                            } else {
                                $.afui.showMask("正在上传!");
                            }

                            //   alert("3")
                            ft.onprogress = function (progressEvent) {
                                if (progressEvent.lengthComputable) {
                                    //  $("#afui_mask h1").html("已上传:"+ (progressEvent.loaded / progressEvent.total*100).toFixed(2) +"%");
                                    // progressStart : function(title, message)
                                    if (localStorage.ios == "0") {
                                        navigator.notification.progressValue((progressEvent.loaded / progressEvent.total * 100).toFixed(2));
                                    } else {
                                        $("#afui_mask h1").html("已上传:" + (progressEvent.loaded / progressEvent.total * 100).toFixed(2) + "%");
                                    }
                                    console.log("正在上传:" + progressEvent.loaded / progressEvent.total);
                                    // loadingStatus.setPercentage(progressEvent.loaded / progressEvent.total);
                                } else {
                                    // $.afui.hideMask();
                                    console.log("正在上传?????");
                                    // loadingStatus.increment();
                                }
                            };
                            ft.upload(success, url,
                                function (success) {
                                    if (localStorage.ios == 1) {
                                        $.afui.hideMask();
                                    } else {
                                        navigator.notification.progressStop();
                                    }


                                    var jsonstr = JSON.parse(success.response);
                                    $("#" + showimgid).attr("src", jsonstr.imgurl);
                                    $("#" + imgdid).val(jsonstr.did);

                                    if (funstr != '' || funstr != 'undefined') {
                                        funstr = funstr.replace("'did'", "'" + jsonstr.did + "'");
                                        console.log(funstr + jsonstr.did + jsonstr.imgurl);
                                        eval(funstr);
                                    }
                                },
                                function (error) {
                                    if (localStorage.ios == 1) {
                                        $.afui.hideMask();
                                    } else {
                                        navigator.notification.progressStop();
                                    }
                                    navigator.notification.alert("上传失败" + error, function () { }, "提示", "确定");
                                    //  alert("上传失败:"+error);
                                },
                                options,
                                true);
                        },
                        function (error) {

                            if (localStorage.ios == 0) {
                                navigator.notification.progressStop();
                                navigator.notification.activityStop();
                            }

                            $.afui.hideMask();
                            alert(error);
                        },
                        data);
                },
                function (error) {
                    $.afui.hideMask();
                    // function(message, completeCallback, title, buttonLabel) {
                    navigator.notification.alert("图片选取失败", function () { }, "提示", "确定");
                },
                // {'sourceType':'0'}
                { 'sourceType': sourceType, 'PICTURE': '0' }
            )


            //   },"提示",["相册","相机","取消"]);
        }, "提示", btnarr);


        return;
        //android
        // window.plugins.CameraPhoto.getPhoto(url, function (success) {
        window.plugins.CameraPhoto.getPhoto(data, function (success) {
            jsonstr = JSON.parse(success);
            $("#" + showimgid).attr("src", jsonstr.imgurl);

            $("#" + imgdid).val(jsonstr.did);

            if (funstr != '' || funstr != 'undefined') {
                funstr = funstr.replace("'did'", "'" + jsonstr.did + "'");
                console.log(funstr + jsonstr.did + jsonstr.imgurl);
                eval(funstr);
            }

        }, function (fail) {
            mylert(fail);
        });
    } else { //ios

        window.plugins.UploadField.upload(url, function (success) {
            //alert(success);
            jsonstr = JSON.parse(success);
            $("#" + showimgid).attr("src", jsonstr.imgurl);
            $("#" + imgdid).val(jsonstr.did);
            if (funstr != '' || funstr != 'undefined') {
                funstr = funstr.replace("'did'", "'" + jsonstr.did + "'");
                console.log(funstr);
                eval(funstr);
            }
        }, function (fail) {
            // alert(fail);
            mylert(fail);
        });
    }
    return jsonstr;

}